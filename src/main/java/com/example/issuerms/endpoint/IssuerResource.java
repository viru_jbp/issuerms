package com.example.issuerms.endpoint;


import com.example.issuerms.domain.AvailableBook;
import com.example.issuerms.domain.Book;
import com.example.issuerms.domain.Issuer;
import com.example.issuerms.repo.IssuerRepo;
import netscape.javascript.JSObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.client.circuitbreaker.ReactiveCircuitBreakerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

@RestController
public class IssuerResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(IssuerResource.class);
    @Autowired(required=true)
    @Qualifier("webClientBook")
    private WebClient webClientBook;

    @Autowired
    private IssuerRepo issuerRepo;

    @Autowired
    private ReactiveCircuitBreakerFactory circuitBreakerFactory;

    @GetMapping("/getAvailableBooks")
    public Mono<ResponseEntity<List<AvailableBook>>> getAvailableBooks() {
//        List<User>.class;
        LOGGER.info("issuerms is calling bookms");
        Mono<ResponseEntity<List<AvailableBook>>> responseEntityMono = webClientBook.get().uri("/allAvailableBooks").retrieve().toEntity(
                new ParameterizedTypeReference<List<AvailableBook>>() {
                }
        ).transform(it -> circuitBreakerFactory.create("backendB")
                .run(it, throwable -> {
                    AvailableBook availableBook = new AvailableBook(); // or use a cache or call backupms
                    availableBook.setAuthor("Fallback Book");
                    return Mono.just(ResponseEntity.ok((List.of(availableBook))));
                }));
        return responseEntityMono;
    }

    @GetMapping("/getNoOfBookAvailableById/{id}")
    public Mono<ResponseEntity<Integer>> getBookById(@PathVariable Integer id) {
//        List<User>.class;
        LOGGER.info("issuerms is calling bookms for single book " + id);
        Mono<ResponseEntity<Integer>> responseEntityMono = webClientBook.get().uri("/availableBook/" + id).retrieve().toEntity(
                new ParameterizedTypeReference<Integer>() {
                }
        ).transform(it -> circuitBreakerFactory.create("backendB")
                .run(it, throwable -> {
                   // AvailableBook availableBook = new AvailableBook(); // or use a cache or call backupms
                    //availableBook.setAuthor("Fallback Book");
                    return Mono.just(ResponseEntity.ok(0));
                }));
        return responseEntityMono;
    }


    @PutMapping("/issueBook/{id}")
    public Mono<ResponseEntity<Integer>> issueBookById(@PathVariable Integer id, String custId, Integer noOfCopies) {
        LOGGER.info("Issue book by id " + id);
        LOGGER.info("No of copies in issuerms " + noOfCopies);
        Mono<ResponseEntity<Integer>> responseEntityMono = webClientBook.get().uri("/availableBook/" + id).retrieve().toEntity(
                new ParameterizedTypeReference<Integer>() {
                }
        ).transform(it -> circuitBreakerFactory.create("backendB")
                .run(it, throwable -> {
                    return Mono.just(ResponseEntity.ok((0)));
                }));
        if(responseEntityMono.block().getBody() > noOfCopies){
            Mono<ResponseEntity<Book>> responseEntityMonoUpdate = webClientBook.put().uri("/updateIssueBook/" + id + "?noOfCopies=" + noOfCopies).retrieve().toEntity(
                    new ParameterizedTypeReference<Book>() {
                    }
            ).transform(it -> circuitBreakerFactory.create("backendB")
                    .run(it, throwable -> {
                        Book book = new Book();
                        book.setTitle("Fallback book");
                        return Mono.just(ResponseEntity.ok((book)));
                    }));
            Issuer issuer = new Issuer();
            Book issueBook = responseEntityMonoUpdate.block().getBody();
            issuer.setNoOfCopies(noOfCopies);
            issuer.setCustId(custId);
            LOGGER.info("ISBN in issuerms " + issueBook.getIsbn());
            issuer.setIsbn(issueBook.getIsbn());
            issuerRepo.save(issuer);
        }
        return responseEntityMono;
    }

    @GetMapping("getAllCustomerIssuedBook")
    public List<Issuer> getAllCustomerIssuedBook(){
        return issuerRepo.findAll();
    }


//    private Mono<List<AvailableBook>> convert(Mono<List<Book>> bookDto) {
//        return bookDto.transform(bookDto1 -> bookDto2).map(
//                books -> {
//                    List<AvailableBook> availableBookDto = new ArrayList<AvailableBook>();
//                    availableBookDto.setAuthor(books.getAuthor());
//                    availableBookDto.setAvailableCopies(books.getTotalCopies() - books.getIssuedCopies());
//                    return availableBookDto;
//                }
//        );
//    }

}
