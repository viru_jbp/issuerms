package com.example.issuerms.repo;


import com.example.issuerms.domain.Issuer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IssuerRepo extends JpaRepository<Issuer, Integer> {
}
