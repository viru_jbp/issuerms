package com.example.issuerms.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class HttpClientConfig {

    @Bean("webClientBookBuilder")
    @LoadBalanced
    public WebClient.Builder webClientBuilderStudent() {
        return WebClient.builder().baseUrl("http://bookms");
    }

    @Bean("webClientBook")
    public WebClient webClientStudent(@Qualifier("webClientBookBuilder") WebClient.Builder studentBuilder) {
        return studentBuilder.build();
    }
}
